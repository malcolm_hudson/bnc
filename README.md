# Instructions for loading package bnc

1. **bnc** is an *R package* for MPL estimation of bivariate normal censored (BNC) linear models. Code is available at the following location, [url](bitbucket.org/malcolm_hudson/bnc). From this `bitbucket` location you can clone it to a local folder of your own, should you wish. In this cloned folder, you will find the file `.Rproj`. Open this in `Rstudio`. 

2. An alternative to install the package in the Rstudio console:  

    ```r
    devtools::install_bitbucket(bnc,build_vignettes=TRUE)
    ```

3. In  the`Rstudio` console:

    `library(devtools); library(usethis)`; `load_all()`
    
    You may be instructed to install standard R packages needed by ``bnc``
    
    Then``load_vignettes()``

   *Help* is obtained by:

- `?bnc`

- ``help(bnc)````

- ``help(bnc-package)``

   An *example dataset*  (Head and Neck cancer competing outcomes) is available:

   `help(HN2)`

   Code for vignettes is available in subfolder **vignettes/**. The vignette **HNexample.Rmd** illustrates application of **bnc** in the example dataset of our paper. 

